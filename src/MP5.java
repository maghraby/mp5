import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * @author : Ahmed Elmaghraby
 */

public class MP5 {


	public static void main(String[] args) throws FileNotFoundException{

		long startTime = System.currentTimeMillis();

		// building the parser from the 1st argument from the command line
		CharacterParser theParser = new CharacterParser(args[0]);

		ArrayList<Character> chars = (ArrayList<Character>) theParser.getCharacters();
		long endTime = System.currentTimeMillis();
		// calculates the time taken for parsing the file
		System.out.println("Parsing took " + (endTime - startTime) + " milliseconds");

		CharacterGraph theGraph = new CharacterGraph(chars);
		// calculates the time taken for building the whole graph
		System.out.println("done");
		endTime = System.currentTimeMillis();
		System.out.println("It took " + (endTime - startTime) + " milliseconds");


		// vertices are 2nd and 3rd argument from the command line
		String vertex1 = args[1];
		String vertex2 = args[2];



		ArrayList<Character> thePath = (ArrayList<Character>) theGraph.findShortestPath(vertex1, vertex2); 

		System.out.println("The shortest path is :");
		// if path was not found, print this
		if (thePath == null){
			System.out.println("No path has been found!");
		}
		// if path was found
		else {
			// print the first node
			System.out.println(vertex1);
			// print the following nodes till the last one
			for (int i = 0;i<thePath.size();i++){

				System.out.println(thePath.get(i).getName());

			}

		}




	}


}
