import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/*
 * @author: Ahmed Elmaghraby
 */

public class CharacterParser {
	private String fileName;
	private ArrayList<Character> characterList;

	/**
	 * Constructer for building the parser object
	 * @param fileName file name to build the parser from
	 */
	public CharacterParser(String fileName){
		this.fileName = fileName;
		characterList = new ArrayList<Character>();
	}

	/**
	 * returns a list of characters that were parsed
	 * @return	a list of characters from the parser
	 * @throws FileNotFoundException if file has not been found 
	 */

	public List getCharacters() throws FileNotFoundException{

		ArrayList<Character> characterTracker = new ArrayList<Character>();

		// starting the scanner
		Scanner reader = new Scanner(new File(fileName));


		// loop for every line
		while (reader.hasNextLine()){
			// assuming character does not exist
			boolean characterExists = false;
			//reads next line
			String text = reader.nextLine();
			// returning a list that contains the character and its comic
			ArrayList<String> textContents = (ArrayList<String>) getText(text);
			// creating the character
			Character marvelCharacter = new Character(textContents.get(0),textContents.get(1));

			// if the tracking list was initially empty then we just add the character
			if ( characterTracker.isEmpty()){
				characterTracker.add(marvelCharacter);
			}
			else {
				// loop to check if we can find the character in the tracker list
				for (int i = 0; i<characterTracker.size();i++){
					
					if (characterTracker.get(i).equals(marvelCharacter)){

						characterTracker.get(i).addBook(textContents.get(1));
						// if found, we set this boolean to true
						characterExists = true;
					}

				}
				// if the checking boolean was still false then it was not found
				if ( characterExists == false)

					characterTracker.add(marvelCharacter);



			}		

		}
		// closing the scanner
		reader.close();
		
		
		characterList = new ArrayList<Character>(characterTracker);



		return characterList;

	}
	/**
	 * method for parsing a line 
	 * @param text a line taken from the file
	 * @return a list that contains the character and the comic
	 */
	private List<String> getText(String text){

		// finding the first quotation mark
		int indexFirstQuot = text.indexOf("\"");
		
		// removing the first quotation mark
		text = text.substring(indexFirstQuot + 1);
		
		//	find the closing quotation mark
		int indexLastQuot = text.indexOf("\"");
		
		// storing the character name in a string
		String characterName = text.substring(0, indexLastQuot);
		
		
		text = text.substring(indexLastQuot + 1);
		
		// find the second quotation mark
		indexFirstQuot = text.indexOf("\"");
		// removing the mark
		text = text.substring(indexFirstQuot + 1);
		
		//finding the closing mark
		indexLastQuot = text.indexOf("\"");
		// storing the comic name
		String comicName = text.substring(0, indexLastQuot);

		
		ArrayList<String> contents = new ArrayList<String>();
		// adding both name and comic ( one in index 0 and one in index 1 )
		contents.add(characterName);
		contents.add(comicName);

		return contents;

	}

}
