import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


/*
 * @author: Ahmed Elmaghraby
 * Class character contains the objects of the marvel characters and their books
 */

public class Character implements Comparable<Character>{
	private String name;
	private Set<String> comicsList;
	private ArrayList<Edge> connections;

	/**
	 * 
	 * Builds a character with the given name and at least one comicBook
	 * 
	 * @param name  name of the character
	 * @param comicBook  name of the comic book
	 * 
	 * @requires  both name and comicBook must not be null
	 * 
	 */
	public Character(String name,String comicBook){

		this.name = name;
		this.comicsList = new TreeSet<String>();
		comicsList.add(comicBook);
		this.connections = new ArrayList<Edge>();

	}

	/**
	 * adds a comic book to the list of comic books that belongs to a certain character
	 * 
	 * @param comicBook name of the comicBook (String)
	 * @requires  comicBook must not be null
	 * @modifies it modifies the list of comicBooks by adding a book to that list.
	 */

	public void addBook (String comicBook){
		comicsList.add(comicBook);

	}

	/**
	 * removes a comic book to the list of comic books that belongs to a certain character
	 * 
	 * @param comicBook  name of the comicBook (String)
	 * @requires  comicBook must not be null
	 * @modifies it modifies the list of comicBooks by removing a book to that list.
	 */

	public void removeBook (String comicBook){

		comicsList.remove(comicBook);

	}

	/**
	 * Adds an edge to the list of connections for the character
	 * 
	 * @param marvelBook : an Object of type Edge
	 * 
	 * @modifies the method modifies the list of connections by adding an Edge to that list.
	 */

	public void addConnection (Edge marvelBook){

		connections.add(marvelBook);
		Collections.sort(connections);

	}
	/**
	 * 
	 * @return
	 */
	public ArrayList getConnections(){
		return connections;
	}

	/**
	 * this method returns a set of the comicsList instead of type List
	 * @return a set of comicsList
	 */

	public Set<String> getSet(){

		return comicsList;

	}

	/**
	 * this method returns the name of the Character
	 * @return name of the Character
	 */

	public String getName(){
		return name;
	}

	/**
	 * this method is auto generated and it overrides the method of hashCode
	 *  by providing a hash code depending on the name.
	 *  
	 *  @return hash code of type integer
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public int compareTo(Character marvelChar){
		return this.getName().compareTo(marvelChar.getName());
	}
	/**
	 * this method is auto generated and it overrides the method of equals.
	 * 
	 * @return True if two objects are true and false otherwise
	 * 
	 * 2 objects of Type Character are considered true if their names are equal
	 * 
	 * note: this method is internal in the package so the equality is for implementation purposes
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Character other = (Character) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * this method overrides the method toString
	 * 
	 * @return string of an object of type Character
	 * 
	 * the representation of the object is to print its name, comics, and edges connected to.
	 */

	@Override
	public String toString() {
		return name;
	}
















}
