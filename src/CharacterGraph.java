import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

/*
 *@author: Ahmed Elmaghraby 
 * 
 */

public class CharacterGraph {
	/**
	 * Represent a universe using an undirected simple graph 
	 * 
	 * Application Programming Interface:
	 * 
	 * Abstraction Function:
	 * - universe is a list of nodes and a list of edges 
	 * 		*list of nodes represents all the character in the universe
	 * 		each node has a list of edges representing all connections they have
	 * 				to other characters in the universe
	 * 		each edge has a source, a target, and 
	 * 				the comic containing source and target
	 * 
	 * 		*List of edges is all existing edges
	 * 		/represents all the comics that exist in universe
	 * 
	 * Representation Invariant:
	 * - Universe != null
	 * - ListofCharacter != null
	 * - for each character:
	 * 		name != null
	 * 		comicsList != null
	 * - ListofEdge != null
	 * - for each edge:
	 * 		target, source, comicBook != null
	 * 		target != source
	 * 		/it is possible for the same edge to have different comicBooks
	 * 
	 * 
	 *
	 */


	private ArrayList<Character> nodes;

	private ArrayList<Edge> edges;

	private ArrayList<Character> visitedNodes;

	/**
	 * create a universe using a list of characters
	 * @param listOfChars a list containing the characters
	 */
	public CharacterGraph(List<Character> listOfChars){

		// add the list to our list of nodes
		nodes = new ArrayList<Character>(listOfChars);
		// sorts the list
		Collections.sort(nodes);

		// initiating a new list for tracking the visited nodes
		visitedNodes = new ArrayList<Character>();

		// loop for making the edges
		for (int i = 0; i<nodes.size();i++){
			// adds the current node to the list of visited nodes
			visitedNodes.add(nodes.get(i));
			// adding an edge
			addEdge(nodes.get(i),nodes);


		}




	}
	/**
	 * Adds edge given a source node and a list of potential target nodes
	 *
	 * - A marvel character from the list is a target node if 
	 * 		it shares comic books with the source node
	 * - All edges between the source and a target includes
	 * 		only the comicBook with the lexicographically lowest name
	 * 		(in case they appear together more than once)
	 * 
	 * @param character is a marvel character (ie. source)
	 * @param listofChar is list of marvel characters other than the character
	 * 				in which we will establish connections to (i.e. targets)
	 */
	private void addEdge(Character marvelChar1, List<Character> listOfChars){

		// a loop to iterate over all the list
		for (int k = 0;k<listOfChars.size();k++){

			// a boolean to check if it was found
			boolean found = false;

			// only works if 2 characters are different
			if(!marvelChar1.equals(listOfChars.get(k)) ){

				// makes a set from the first character's list of comics
				Set<String> setOfComics = new TreeSet<String>(marvelChar1.getSet());
				// intersects both lists in a set
				setOfComics.retainAll(listOfChars.get(k).getSet());


				// as long as this set is not empty
				if (!setOfComics.isEmpty()){
					// store the set in a list
					ArrayList<String> listOfComics = new ArrayList<String>(setOfComics);

					// make a new edge 
					Edge edge = new Edge(listOfChars.get(k),listOfComics.get(0));

					// add this edge to the character
					marvelChar1.addConnection(edge);


				}


			}

		}
	}


	/**
	 * uses multi-threaded breadth-first search (bfs) -- this implementation uses only a single thread
	 * i.e. visit all of source's connections
	 * 		starting from nearest adjacent and expand outwards
	 * 		until target is found 
	 * 		or all connections have been visited
	 * 
	 * @param source node != null
	 * @param target node != null
	 * @return shortest path between two characters if it exists;
	 * 			if character pair has multiple paths, 
	 * 			return the lexicographically least path
	 * @throws noPathException if no path exists
	 */
	public List<Character> findShortestPath(String firstChar, String secondChar){

		// Setup:
		Character start = null;
		Character destination = null;

		// list of path
		ArrayList<Character> initialList = new ArrayList<Character>();

		// map nodes to paths, key = visited node, value = path from source to current node
		Map<Character,List<Character>> theMap = new HashMap<Character,List<Character>>();

		// nodes to visit
		Queue<Character> theQueue = new LinkedList<Character>();

		// finding first and second characters

		for (int i = 0; i < nodes.size();i++){

			if (nodes.get(i).getName().equals(firstChar)){
				start = nodes.get(i);
			}

			if (nodes.get(i).getName().equals(secondChar)){
				destination = nodes.get(i);
			}

		}

		if(start != null && destination != null){
			// add source to queue and map to empty list
			theQueue.add(start);
			theMap.put(start, initialList);

		}

		//graph traversal
		while (!theQueue.isEmpty()){

			Character connectedNode = null;
			// currentNode = next node in queue
			Character currentNode = theQueue.poll();
			
			// if next up is destination, return path
			if (currentNode.equals(destination))
				return theMap.get(currentNode);

			// map all connections of the current node
			for (int i = 0; i< currentNode.getConnections().size();i++){

				// connected node = target of the currentNode's current edge
				connectedNode = ((Edge) currentNode.getConnections().get(i)).getTarget();



				// if the target node has not been visited:
				if (!theMap.containsKey(connectedNode)){

					ArrayList<Character> path = (ArrayList<Character>) theMap.get(currentNode);

					ArrayList<Character> newPath = new ArrayList<Character>(path);
					
					//append the new path to the old path + connected node
					//and map this path to the new node
					// then set the target to be the next node
					newPath.add(connectedNode);

					theMap.put(connectedNode, newPath);

					theQueue.add(connectedNode);
				}
			}
		}


		return null;

	}

	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * print the graph in terms of all the characters
	 */
	@Override
	public String toString() {
		return "CharacterGraph [graph=" + nodes + "]";
	}

}
