import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class TestingGraph {

	@Test
	public void test() {
		
		// defining characters
		
		Character char1 = new Character("A","1");
		char1.addBook("2");
		
		Character char2 = new Character("B","1");
		char2.addBook("3");
		
		Character char3 = new Character("C","1");
		char3.addBook("4");
		
		Character char4 = new Character("D","2");
		char4.addBook("5");
		char4.addBook("6");
		
		Character char5 = new Character("E","3");
		char5.addBook("4");
		char5.addBook("7");
		
		Character char6 = new Character("F","7");
		char6.addBook("5");
		char6.addBook("8");
		char6.addBook("10");
		
		Character char7 = new Character("G","6");
		char7.addBook("10");
		
		Character char8 = new Character("H","8");
		
		ArrayList<Character> theList = new ArrayList<Character>();
		
		theList.add(char1);
		theList.add(char4);
		theList.add(char8);
		theList.add(char3);
		theList.add(char7);
		theList.add(char5);
		theList.add(char6);
		theList.add(char2);
		
		
		CharacterGraph newGraph = new CharacterGraph(theList);
		
		//System.out.println(newGraph);
		
		ArrayList<Character> path = (ArrayList<Character>) newGraph.findShortestPath("A", "H");
		
		System.out.print("A >> ");
		
		
		for (int i = 0; i < path.size();i++){
			
			if (i == path.size()-1)
			
				System.out.print(path.get(i).getName());
			else {
				System.out.print(path.get(i).getName()+" >> ");
				
			}
		}
		
		
		
		
	}

}
