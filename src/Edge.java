/*
 * @author: Ahmed Elmaghraby
 * 
 */

public class Edge implements Comparable<Edge> {
	
	
	private Character target;
	private String comicBook;
	
	/**
	 * constructer for building a new edge
	 * @param target node
	 * @param comicBook the target and source appeared in
	 */

	public Edge (Character target, String comicBook){
		this.target = target;
		this.comicBook = comicBook;
	}


	
	
	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * 
	 * prints out the target info of this edge
	 */

	@Override
	public String toString() {
		return "\nEdge ["+ "target=" + target.getName() +"]\n";
	}
	
	/**
	 * method for returning the target node
	 * @return the target node
	 */	
	public Character getTarget(){
		return target;
	}
	
	/**
	 * method for overriding the compareTo 
	 * compare the target-name of this edge to target-name of that edge
	 */
	@Override
	public int compareTo(Edge edge){
		
		return this.getTarget().getName().compareTo(edge.getTarget().getName());
		

	}



	/** (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * 
	 * returns the unique id of hash code
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}


	
}
